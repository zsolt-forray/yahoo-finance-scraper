FROM python:3

COPY requirements.txt /
RUN pip install -r requirements.txt

COPY yahoo_finance_scraper.py /

CMD [ "python", "./yahoo_finance_scraper.py" ]

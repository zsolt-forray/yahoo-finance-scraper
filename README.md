# Yahoo Finance Scraper

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/82140bfe9fcb4f3081e71460d6b38b67)](https://www.codacy.com/manual/forray.zsolt/yahoo-finance-scraper?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Zsolt-Forray/yahoo-finance-scraper&amp;utm_campaign=Badge_Grade)
[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-370/)

## Description
This project includes Python scraper to extract financial information of stocks from the `Yahoo! Finance` website.

## Usage
`Data Source: https://finance.yahoo.com/`

![Screenshot](/png/source.png)

### Usage Example
Collect all available financial data of Citigroup (C).

```python
#!/usr/bin/python3

import yahoo_finance_scraper as yfs

sc = yfs.YahooFinanceScraper("C")
res = sc.run_app()

print(res)
```

### Output
Financial data in JSON format.

![Screenshot](/png/target.png)

## LICENSE
MIT

## Contributions
Contributions to this repository are always welcome.
This repo is maintained by Zsolt Forray (forray.zsolt@gmail.com).

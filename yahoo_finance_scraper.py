#!/usr/bin/python3


"""
YahooFinanceScraper scrapes and downloads financial data from Yahoo! Finance
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '28/11/2019'
__status__  = 'Development'


from bs4 import BeautifulSoup as bs
import requests
import re
from datetime import datetime


class YahooFinanceScraper:
    def __init__(self, ticker):
        self.ticker = ticker
        self.res_dict = {}
        self.res_dict[self.ticker] = dict(price={}, volume={}, dividend={},
                                          market_cap={}, measures={}, earnings={})

        self.url    = "https://finance.yahoo.com/quote/{}?".format(self.ticker)
        self.params = dict(p="{}".format(self.ticker))

    def get_soup(self):
        # Get BeautifulSoup object
        CONNECT_TIMEOUT = 10
        READ_TIMEOUT = 10
        self.req = requests.get(url=self.url, params=self.params, \
                                timeout=(CONNECT_TIMEOUT, READ_TIMEOUT))
        self.soup = bs(self.req.text, "html.parser")

    def get_raw_table(self):
        # Get raw financial data table
        self.table_soup = self.soup.find_all("table")

    def get_raw_data(self):
        self.get_soup()
        self.get_raw_table()
        # Get raw financial data and store them in a dictionary
        raw_data_list = []
        for table in self.table_soup:
            for tr in table.find_all("tr"):
                td_list = [i.text for i in tr]
                raw_data_list.append(td_list)
        self.raw_data_dict = dict(raw_data_list)

    @staticmethod
    def regex_patterns(data):
        pattern = {
                    "fv"  : "\d+\.\d+|\d+",
                    "iv"  : "\d+",
                    "exd" : "\d{4}\-\d{2}\-\d{2}",
                    "ernd": "\D{3}\s\d{1,2}\s\d{4}",
                    "nfv" : "[-]?\d+\.\d+|[-]?\d+",
                    "str" : "^[a-zA-Z0-9.]+"
                    }
        return pattern[data]

    @staticmethod
    def add_format(value, pattern_id):
        if pattern_id == "fv" or pattern_id == "nfv":
            return float(value)
        elif pattern_id == "iv":
            return int(value)
        elif pattern_id == "ernd":
            value = datetime.strptime(value, "%b %d %Y").date()
            return datetime.strftime(value, "%Y-%m-%d")
        else:
            return value

    def get_data(self, yahoo_id, pattern_id, index):
        raw_value = self.raw_data_dict[yahoo_id].replace(",", "")
        pattern = YahooFinanceScraper.regex_patterns(pattern_id)
        return re.findall(pattern, raw_value)[index]

    def update_result_json(self, key, subkey, yahoo_id, pattern_id, index):
        try:
            value = self.get_data(yahoo_id, pattern_id, index)
            value = YahooFinanceScraper.add_format(value, pattern_id)
            self.res_dict[self.ticker][key][subkey] = value
        except Exception:
            self.res_dict[self.ticker][key][subkey] = None

    def run_app(self):
        self.get_raw_data()
        subkeys = {
                "prev_close"    : ("Previous Close", "price", "fv", 0),
                "open_price"    : ("Open", "price", "fv", 0),
                "yr_target"     : ("1y Target Est", "price", "fv", 0),
                "dlow"          : ("Day's Range", "price", "fv", 0),
                "dhigh"         : ("Day's Range", "price", "fv", 1),
                "ylow"          : ("52 Week Range", "price", "fv", 0),
                "yhigh"         : ("52 Week Range", "price", "fv", 1),
                "avg_volume"    : ("Avg. Volume", "volume", "iv", 0),
                "curr_volume"   : ("Volume", "volume", "iv", 0),
                "div_amnt"      : ("Forward Dividend & Yield", "dividend", "fv", 0),
                "div_yield"     : ("Forward Dividend & Yield", "dividend", "fv", 1),
                "pe_ratio"      : ("PE Ratio (TTM)", "measures", "fv", 0),
                "eps"           : ("EPS (TTM)", "measures", "nfv", 0),
                "beta"          : ("Beta (3Y Monthly)", "measures", "nfv", 0),
                "ex_div_date"   : ("Ex-Dividend Date", "dividend", "exd", 0),
                "earnings_date" : ("Earnings Date", "earnings", "ernd", 0),
                "market_cap"    : ("Market Cap", "market_cap", "str", 0),
            }

        for item in subkeys.items():
            subkey     = item[0]
            yahoo_id   = item[1][0]
            key        = item[1][1]
            pattern_id = item[1][2]
            index      = item[1][3]
            self.update_result_json(key, subkey, yahoo_id, pattern_id, index)
        return self.res_dict


if __name__ == "__main__":
    yfs = YahooFinanceScraper("AMAT")
    res = yfs.run_app()
    print(res)
